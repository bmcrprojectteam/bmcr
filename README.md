# README #

### What is this repository for? ###

* Website using Boostrap-Framwork.
* Node.js Backend Application.
* Python Application based on Gensim.
* This Application can be used to determine similarities between Fields of Business Model Canvases or Lean Canvases.
* Therefore the TF-IDF Model / Method is used.
* Version 1.0

### Who do I talk to? ###

* This Application was created as a student group project at Herman Hollerith Centre, Böblingen. (Hochschule Reutlingen)
* This Project is called BMCr - Business Model Crunching.

## For further information check the [Wiki](https://bitbucket.org/bmcrprojectteam/bmcr/wiki/Home) page ##